//
//  WidgetDemoApp.swift
//  WidgetDemo
//
//  Created by Manuel Espeso on 7/12/21.
//

import SwiftUI

@main
struct WidgetDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .onOpenURL { url in
                    maybeOpenedFromWidget(url.scheme)
                }
        }
    }
    
    private func maybeOpenedFromWidget(_ scheme: String?) {
        guard scheme == "widget-deeplink" else { return }
        print("🚀 Launched from widget")
    }
}
