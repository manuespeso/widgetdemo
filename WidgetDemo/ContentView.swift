
import WidgetKit
import SwiftUI

struct ContentView: View {
    
    @State private var count = 0
    private let userDefaults = UserDefaults(suiteName: "group.widgetInformationGroup")
    
    var body: some View {
        VStack {
            Button {
                count += 1
                
                userDefaults?.setValue(count, forKey: "count")
                WidgetCenter.shared.reloadAllTimelines()
            } label: {
                Text("You have been pushed button \(count) times!")
                    .padding()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
