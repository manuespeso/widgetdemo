
import WidgetKit
import SwiftUI

///Este provider se encarga de indicar cual es la informacion que vamos a tener en el widget y cuando se va a actualizar
struct WeatherProvider: TimelineProvider {
    
    private let userDefaults = UserDefaults(suiteName: "group.widgetInformationGroup")
    
    ///Placeholder que mostrar
    func placeholder(in context: Context) -> WeatherEntry {
        WeatherEntry(date: Date(),
                     degrees: 2,
                     title: "Loading",
                     desc: "Lorem ipsum",
                     icon: "thermometer")
    }
    /// Placeholder que se visualiza en el momento en el que el usuario esta eligiendo el widget antes de añadirlo a su pantalla
    func getSnapshot(in context: Context, completion: @escaping (WeatherEntry) -> ()) {
        let entry = WeatherEntry(date: Date(),
                                 degrees: 6,
                                 title: "Eligeme",
                                 desc: "Soy muy chulo",
                                 icon: "thermometer")
        completion(entry)
    }
    ///Datos reales para visualizar con su correspondiente tasa de refresco
    func getTimeline(in context: Context, completion: @escaping (Timeline<WeatherEntry>) -> ()) {
        let count = userDefaults?.value(forKey: "count") as? Int ?? 0
        
        let entries: [WeatherEntry] = [
            WeatherEntry(date: Date(),
                         degrees: 5,
                         title: "Nieve",
                         desc: "Parece que va a nevar",
                         icon: "snowflake"),
            WeatherEntry(date: Date().addingTimeInterval(5),
                         degrees: 20,
                         title: "Lluvia",
                         desc: "Parece que va a llover",
                         icon: "cloud.bolt.rain"),
            WeatherEntry(date: Date().addingTimeInterval(10),
                         degrees: 35,
                         title: "Sol",
                         desc: "\(count)",
                         icon: "sun.max.fill")
            
        ]
        
        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct WeatherEntry: TimelineEntry {
    ///Variable obligatoria para que el widget sepa cada cuanto tiempo tiene que actualizarse
    let date: Date
    ///Variables extras
    let degrees: Int
    let title: String
    let desc: String
    let icon: String
}

struct MyFirstWidgetEntryView : View {
    var entry: WeatherProvider.Entry
    @Environment(\.widgetFamily) var family: WidgetFamily //custom UI based on widget family
    
    private let deeplinkURL = URL(string: "widget-deeplink://")!
    
    var body: some View {
        VStack {
            Image(systemName: entry.icon)
                .resizable()
                .frame(width: 50, height: 50, alignment: .center)
            Text("\(entry.degrees) ºC")
                .font(.title2)
            Text(entry.date, style: .time)
            Text(entry.desc)
                .multilineTextAlignment(.center)
                //.isHidden(family == .systemSmall)
        }
        .widgetURL(deeplinkURL)
    }
}

struct MyFirstWidget: Widget {
    let kind: String = "MyFirstWidget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: WeatherProvider()) { entry in
            MyFirstWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("Mi primer widget")
        .description("Este es mi primer widget de ejemplo.")
        //.supportedFamilies([.systemMedium])
    }
}


@main
struct WidgetsBudle: WidgetBundle {
    var body: some Widget {
        MyFirstWidget()
        //Widget2()
    }
}

struct MyFirstWidget_Previews: PreviewProvider {
    static var previews: some View {
        MyFirstWidgetEntryView(entry: WeatherEntry(date: Date(),
                                                   degrees: 10,
                                                   title: "Luvia",
                                                   desc: "Parece que va a llover",
                                                   icon: "cloud.rain.fill"))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
